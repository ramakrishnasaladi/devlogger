import { Component, OnInit, ViewChild, EventEmitter, Output, Inject, Input } from '@angular/core';
import { Post } from 'src/app/models/post';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @ViewChild('postForm')
  postForm: any;
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  @Input() currentPost: Post;
  @Input() isEdit: boolean;
  constructor(private postService: PostsService) { }

  ngOnInit(): void {
  }

  addPost() {

    this.postService.addPost(this.currentPost as Post).subscribe(post => {
      this.newPost.emit(post);
    })
    this.postForm.reset();

  }

  updatePost() {
    this.postService.updatePost(this.currentPost as Post).subscribe(post => {
      this.updatedPost.emit(post);
    })
  }
}
