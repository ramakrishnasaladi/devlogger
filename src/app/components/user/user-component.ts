import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user'

@Component({
  selector: 'app-user',
  templateUrl: './user-template.html',
  styleUrls: ['./user-style.css']
})
export class UserComponent implements OnInit {
  // properties
  user: User;

  // firstName: string;
  // lastName: string;
  // age: number;
  // address;

  // constructor
  constructor() {

  }

  ngOnInit() {
    this.user = {
      firstName: 'Rama Krishna',
      lastName: 'Saladi',
      age: 25,
      email: 'Rama@abc.com',
    }
  }

  sayHello() {
    console.log(`Hello ${this.user.firstName}`);
  }
}
