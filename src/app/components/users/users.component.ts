import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../models/user';

import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user: User = {
    firstName: '',
    lastName: '',
    age: null,
    email: '',
  }
  users: User[];
  showExpanded: boolean = true;
  loaded: boolean = true;
  enableADD: boolean = true;
  showUserForm: boolean = false;
  @ViewChild('userForm')
  form: any;
  data: any;

  constructor(private userService: UserService) {
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngInit');
    this.userService.getUsers().subscribe(users => {
      this.users = users
    });
    this.loaded = true;
    this.userService.getData().subscribe(data => {
      console.log(data);
    });
  }

  toggleHide(user: User): void {
    user.hide = !user.hide;
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    if (!valid) {
      console.log('Form is not valid');
    } else {
      value.isActive = true;
      value.registered = new Date();
      value.hide = true;
      this.userService.addUser(value);
      this.form.reset();
    }
  }
}
