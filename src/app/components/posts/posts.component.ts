import { Component, OnInit } from '@angular/core';

import { PostsService } from "../../services/posts.service";
import { Post } from "../../models/post";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: Post[];
  currentPost: Post = {
    id: 0,
    title: '',
    body: ''
  };
  isEdit: boolean = false;

  constructor(private postsService: PostsService) { }

  onNewPost(post: Post) {
    console.log(post);
    this.posts.unshift(post);
  }

  editPost(post: Post) {
    this.currentPost = post;
    this.isEdit = true;
  }

  updatedPost(post: Post) {
    this.posts.forEach((item, index) => {
      if (post.id == item.id) {
        this.posts.splice(index, 1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        }
      }
    })
  }

  ngOnInit(): void {
    this.postsService.getPosts().subscribe(posts => {
      this.posts = posts;
    })
  }

  removePost(post: Post) {
    console.log(post);
    this.postsService.deletePost(post).subscribe(() => {      
      this.posts.forEach((item, index) => {
        if (post.id == item.id) {
          this.posts.splice(index, 1);
          this.isEdit = false;
          this.currentPost = {
            id: 0,
            title: '',
            body: ''
          }
        }
      })
    })
  }

}
