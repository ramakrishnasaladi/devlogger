import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";

import { Post } from "../models/post";

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable()
export class PostsService {

  postsURL: string = "https://jsonplaceholder.typicode.com/posts";

  constructor(private httpClient: HttpClient) { }

  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.postsURL);
  }

  addPost(post: Post): Observable<Post> {
    return this.httpClient.post<Post>(this.postsURL, post, httpOptions);
  }

  updatePost(post: Post): Observable<Post> {
    const url = `${this.postsURL}/${post.id}`;
    return this.httpClient.put<Post>(url, post, httpOptions);
  }

  deletePost(post: Post): Observable<Post> {
    const url = `${this.postsURL}/${post.id}`;
    return this.httpClient.delete<Post>(url, httpOptions);
  }
}
