import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";

import { User } from "../models/user";

@Injectable()
export class UserService {

  users: User[];
  data: Observable<any>;

  constructor() {
    this.users = [
      {
        firstName: 'Rama Krishna',
        lastName: 'Saladi',
        age: 25,
        email: 'Rama@xyz.com',
        isActive: true,
        registered: new Date('10/26/1990 12:00:00'),
        hide: true
      }, {
        firstName: 'Virat',
        lastName: 'Kohli',
        age: 25,
        email: 'Virat@xyz.com',
        isActive: true,
        registered: new Date('08/26/1990 12:00:00'),
        hide: true
      }, {
        firstName: 'Sachin',
        lastName: 'Tendulkhar',
        age: 25,
        email: 'Sachin@xyz.com',
        isActive: false,
        registered: new Date('12/26/1990 12:00:00'),
        hide: true
      }
    ]
  }

  getData() {
    this.data = new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      }, 1000)
      setTimeout(() => {
        observer.next(2);
      }, 2000)
      setTimeout(() => {
        observer.next(3);
      }, 3000)
      setTimeout(() => {
        observer.next(4);
      }, 4000)
    });

    return this.data;
  }

  getUsers(): Observable<User[]> {
    return of(this.users);
  }

  addUser(user: User) {
    this.users.unshift(user);
  }
}
